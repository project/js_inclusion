<?php

/**
 * @file
 *
 * Js inclusion settings for panel pages.
 */
 
$plugin = array(
  'single' => TRUE,
  'title' => t('JS inclusion settings'),
  'description' => t('JS inclusion settings - file upload, link and inline js settings'), // Description to show up on the pane screen.
  'category' => t('JS Inclusion'),
  'edit form' => 'js_inclusion_js_inclusion_content_type_edit_form',
  'render callback' => 'js_inclusion_js_inclusion_content_type_render',
  'all contexts' => TRUE,
);

function js_inclusion_js_inclusion_content_type_render($subtype, $conf, $args, $contexts) {
  $default_weight = variable_get('js_inclusion_default_order', TRUE);
  $weight = (!($default_weight) && isset($conf['weight'])) ? $conf['weight'] : 0;
  
  if (isset($conf['inclusion_type']) && ($conf['inclusion_type'] == 'file_upload' || $conf['inclusion_type'] == 'inline_js')) {
    if (isset($conf['inclusion_type_value']) && !empty($conf['inclusion_type_value'])) {
      drupal_add_js($conf['inclusion_type_value'], array(
        'type' => 'inline',
        'scope' => isset($conf['position']) ? $conf['position'] : 'footer',
        'group' => JS_THEME,
        'weight' => $weight,
      ));
    }
  }
  
  if (isset($conf['inclusion_type']) && ($conf['inclusion_type'] == 'link_url')) {
    if (isset($conf['inclusion_type_link_url']) && !empty($conf['inclusion_type_link_url'])) {
      $url = $conf['inclusion_type_link_url'];
      $type = _js_inclusion_validate_url($url);
      
      if ($type == JS_INCLUSION_LINK_EXTERNAL && _js_inclusion_external_url_exist($url) || ($type != JS_INCLUSION_LINK_EXTERNAL && file_exists($url))) {
      
        drupal_add_js($url, array(
          'type' => ($type == JS_INCLUSION_LINK_EXTERNAL) ? $type : 'file',
          'scope' => isset($conf['position']) ? $conf['position'] : 'footer',
          'group' => JS_THEME,
          'weight' => $weight,
        ));
      }
    }
  }
}

function js_inclusion_js_inclusion_content_type_edit_form($form, &$form_state) {
  $preset = isset($form_state['conf']) ? $form_state['conf'] : array();
  
  // Select inclusion type
  $form['inclusion_type'] = array(
    '#type' => 'select',
    '#title' => t('Select JS Inclusion type'),
    '#description' => t('Select Javascript type like file upload / link / inline JS'),
    '#options' => _js_inclusion_get_inclusion_types(),
    '#default_value' => (isset($preset['inclusion_type'])) ? $preset['inclusion_type'] : '',    
  );
  
  $form['inclusion_type_file_upload'] = array(
    '#type' => 'file',
    '#name' => 'files',
    '#title' => t('Upload JS file'),
    '#default_value' => FALSE,
    '#states' => array(
      'visible' => array(
        ':input[name="inclusion_type"]' => array('value' => 'file_upload'),
      ),
    ),   
  );
  
  $form['inclusion_type_link_url'] = array(
    '#type' => 'textfield',
    '#title' => t('Provide Internal / External javascript link'),
    '#description' => t('Online or local link.'),
    '#default_value' => (isset($preset['inclusion_type_link_url'])) ? $preset['inclusion_type_link_url'] : '',     
    '#states' => array(
      'visible' => array(
        ':input[name="inclusion_type"]' => array('value' => 'link_url'),
      ),
    ),
  );
   
  $form['inclusion_type_inline_js_code'] = array(
    '#type' => 'textarea',
    '#title' => t('JS code'),
    '#description' => t('The actual JavaScript code goes in here. There is no need to insert the &lt;script&gt; tags.'),
    '#rows' => 10,
    '#default_value' => (isset($preset['inclusion_type_value'])) ? $preset['inclusion_type_value'] : '',  
    '#states' => array(
      'visible' => array(
        array(
          ':input[name="inclusion_type"]' => array('value' => 'inline_js'),
        ),
        array(
          ':input[name="inclusion_type"]' => array('value' => 'file_upload'),
          ':input[name="inclusion_type_inline_js_code"]' => array('filled' => TRUE),          
        ),               
      ),
    ),
  );
  
  $form['placement'] = array(
    '#type' => 'fieldset',
    '#title' => t('Placement options'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  
  $form['placement']['position'] = array(
    '#type' => 'select',
    '#title' => 'Position of the javascipt',
    '#description' => t('Where in the HTML will the JavaScript be placed.'),
    '#options' => array(
      'header' => t('Header'),
      'footer' => t('Footer'),
    ),
    '#default_value' => (isset($preset['position']) && !empty($preset['position'])) ? $preset['position'] : 'footer',
  );
  
  //Weight field will be used only when default order is disabled  
  for ($inc = -15; $inc <= 15; $inc++) {
    $weight_options[$inc] = $inc;
  }
  $settings_link = t('<br/>Weight property will be used only when default order is disabled. <a href="@inclusion_settings_page">Click here</a> to change the settings.',  array('@inclusion_settings_page' => url('admin/config/development/js-inclusion')));
  
  $form['weight'] = array(
    '#type' => 'select',
    '#title' => t('Weight of the inclusion'),
    '#description' => t('Lower weight is presented on the page before a higher weight.') . $settings_link,
    '#options' => ($weight_options),
    '#default_value' => (isset($preset['weight']) && !empty($preset['weight'])) ? $preset['weight'] : 0,
  );
  
  return $form;    
}

/**
 * Validation handler for the preset edit form.
 */
function js_inclusion_js_inclusion_content_type_edit_form_validate($form, &$form_state) {

  // Check for uploaded file extension and size  
  if ($form_state['values']['inclusion_type'] == 'file_upload') {
    if (empty($_FILES['files']['name']) && empty($form_state['values']['inclusion_type_inline_js_code'])) {
      form_set_error('inclusion_type_file_upload', t('Please upload javascript file to include'));
    }
    elseif (!empty($_FILES['files']['name'])) {
      $file = new stdClass();
      $file->filesize = $_FILES['files']['size'];
      $file->filename = $_FILES['files']['name'];
      $validate_response = file_validate_extensions($file, JS_INCLUSION_ALLOWED_FILE_EXTENSION);
      if (count($validate_response) > 0) {
        form_set_error('inclusion_type_file_upload', implode('<br/>', $validate_response));
      }
      else {
        $validate_response = file_validate_size($file, JS_INCLUSION_ALLOWED_FILE_SIZE);
        if (count($validate_response) > 0) {
          form_set_error('inclusion_type_file_upload', implode('<br/>', $validate_response));
        }
      }
    }
  }
  elseif ($form_state['values']['inclusion_type'] == 'link_url') {
    $url = $form_state['values']['inclusion_type_link_url'];
    $validate_response = _js_inclusion_validate_extension($url, JS_INCLUSION_ALLOWED_FILE_EXTENSION, 'link');
    if (!empty($validate_response)) {
      form_set_error('inclusion_type_link_url', $validate_response);
    }
    else {
      if (url_is_external($url) && !_js_inclusion_external_url_exist($url)) {
        form_set_error('inclusion_type_link_url', 'Please provide a valid external js link. Not able to find the file.');
      }
      elseif (!url_is_external($url) && !file_exists($url)) {
        form_set_error('inclusion_type_link_url', 'Please provide a valid internal js url. Not able to find the file.');
      }
    }
  } 
}

/**
 * Submit handler for the preset edit form.
 */
function js_inclusion_js_inclusion_content_type_edit_form_submit($form, &$form_state) {

  if ($form_state['values']['inclusion_type'] == 'file_upload') {
    if (isset($_FILES["files"]["tmp_name"]) && !empty($_FILES["files"]["tmp_name"])) {
      $form_state['values']['inclusion_type_inline_js_code'] = file_get_contents($_FILES["files"]["tmp_name"]);      
    }
  }  
  elseif ($form_state['values']['inclusion_type'] == 'link_url') {
    $form_state['conf']['inclusion_type_link_url'] = $form_state['values']['inclusion_type_link_url'];
  }  
  elseif ($form_state['values']['inclusion_type'] == 'inline_js') {
    $form_state['values']['inclusion_type_inline_js_code'] = strip_tags($form_state['values']['inclusion_type_inline_js_code']);
  }

  $values = $form_state['values'];
  $form_state['conf']['inclusion_type'] = $values['inclusion_type'];
  $form_state['conf']['inclusion_type_value'] = $values['inclusion_type_inline_js_code'];
  $form_state['conf']['position'] = $values['position'];
  $form_state['conf']['weight'] = $values['weight'];
}
