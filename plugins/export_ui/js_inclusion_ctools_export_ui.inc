<?php

/**
 * Define this Export UI plugin.
 */
$plugin = array(
  'schema' => 'js_inclusion_preset',  
  'access' => 'administer js_inclusion',
  
  'menu' => array(
    'menu item' => 'js_inclusion',
    'menu title' => 'JS Inclusion',
    'menu description' => 'Administer JS Inclusion configuration presets.',
  ),
  
  // Define user interface texts.
  'title singular' => t('JS Inclusion configuration'),
  'title plural' => t('JS Inclusion configurations'),
  'title singular proper' => t('JS Inclusion configuration'),
  'title plural proper' => t('JS Inclusion configurations'), 

  // Define the names of the functions that provide the add/edit forms.
  'form' => array(
    'settings' => 'js_inclusion_ctools_export_ui_form',
    'validate' => 'js_inclusion_ctools_export_ui_form_validate',
    'submit' => 'js_inclusion_ctools_export_ui_form_submit',
  ),
);

/**
 * Define the preset add/edit form.
 */
function js_inclusion_ctools_export_ui_form(&$form, &$form_state) {
  $preset = isset($form_state['item']) ? $form_state['item'] : array();
  
  $form['description'] = array(
    '#type' => 'textfield',
    '#title' => t('Description'),
    '#description' => t('The human readable name or description of this preset.'),
    '#maxlength' => '255',
    '#default_value' => $preset->description,
  );
  
  // Select inclusion type
  $form['inclusion_type'] = array(
    '#type' => 'select',
    '#title' => t('Select JS Inclusion type'),
    '#description' => t('Select Javascript type like file upload / link / inline JS'),
    '#options' => _js_inclusion_get_inclusion_types(),
    '#default_value' => (isset($preset->inclusion_type)) ? $preset->inclusion_type : '',    
  );
  
  $form['inclusion_type_file_upload'] = array(
    '#type' => 'file',
    '#name' => 'files',
    '#title' => t('Upload JS file'),
    '#default_value' => FALSE,
    '#states' => array(
      'visible' => array(
        ':input[name="inclusion_type"]' => array('value' => 'file_upload'),
      ),
    ),
    '#upload_validators' => array(
      'file_validate_size' => array(0),
      'file_validate_extensions' => array('js'),
    ),
  );
  
  $form['inclusion_type_link_url'] = array(
      '#type' => 'textfield',
      '#title' => t('Provide Internal / External javascript link'),
      '#description' => t('Online or local link.'),
      '#maxlength' => '255',
      '#default_value' => (isset($preset->link_url)) ? $preset->link_url : '',     
      '#states' => array(
      'visible' => array(
        ':input[name="inclusion_type"]' => array('value' => 'link_url'),
      ),
    ),      
  );
   
  $form['inclusion_type_inline_js_code'] = array(
    '#type' => 'textarea',
    '#title' => t('JS code'),
    '#description' => t('The actual JavaScript code goes in here. There is no need to insert the &lt;script&gt; tags.'),
    '#rows' => 10,
    '#default_value' => (isset($preset->inline_js)) ? $preset->inline_js : '',  
    '#states' => array(
      'visible' => array(
        array(
          ':input[name="inclusion_type"]' => array('value' => 'inline_js'),
        ),
        array(
          ':input[name="inclusion_type"]' => array('value' => 'file_upload'),
          ':input[name="inclusion_type_inline_js_code"]' => array('filled' => TRUE),          
        ),
               
      ),
    ),
  );
  
  $form['placement'] = array(
    '#type' => 'fieldset',
    '#title' => t('Placement options'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  
  $form['placement']['position'] = array(
    '#type' => 'select',
    '#title' => 'Position of the javascipt',
    '#description' => t('Where in the HTML will the JavaScript be placed.'),
    '#options' => array(
      'header' => t('Header'),
      'footer' => t('Footer'),
    ),
    '#default_value' => (isset($preset->position) && !empty($preset->position)) ? $preset->position : 'footer',
  );
    
  $form['pages'] = array(
    '#type' => 'fieldset',
    '#title' => t('Pages'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $title = t('Pages');
  $description = t("Specify pages by using their paths. Enter one path per line. The '*' character is a wildcard. Example paths are %blog for the blog page and %blog-wildcard for every personal blog. %front is the front page.", array('%blog' => 'blog', '%blog-wildcard' => 'blog/*', '%front' => '<front>'));
  $options = array(
    0 => t('All pages except the listed pages'),
    1 => t('The listed pages only'),
  );  
  $form['pages']['page_visibility'] = array(
    '#type' => 'radios',
    '#title' => t('Add this inclusion to specific pages'),
    '#options' => $options,
    '#default_value' => (isset($preset->page_visibility) && !empty($preset->page_visibility)) ? $preset->page_visibility : 0,
  );
  $form['pages']['page_visibility_pages'] = array(
    '#type' => 'textarea',
    '#title' => $title,
    '#title_display' => 'invisible',
    '#default_value' => (isset($preset->page_visibility_pages) && !empty($preset->page_visibility_pages)) ? $preset->page_visibility_pages : '',
    '#description' => $description,
    '#rows' => 10,
  );
  
  //Weight field will be used only when default order is disabled  
  for ($inc = -15; $inc <= 15; $inc++) {
    $weight_options[$inc] = $inc;
  }
  $settings_link = t('<br/>Weight property will be used only when default order is disabled. <a href="@inclusion_settings_page">Click here</a> to change the settings.',  array('@inclusion_settings_page' => url('admin/config/development/js-inclusion')));
  
  $form['weight'] = array(
    '#type' => 'select',
    '#title' => t('Weight of the inclusion'),
    '#description' => t('Lower weight is presented on the page before a higher weight.') . $settings_link,
    '#options' => ($weight_options),
    '#default_value' => (isset($preset->weight) && !empty($preset->weight)) ? $preset->weight : 0,
  );
}

/**
 * Validation handler for the preset edit form.
 */
function js_inclusion_ctools_export_ui_form_validate($form, &$form_state) {  
    
  // Check for string identifier sanity
  if (!preg_match('!^[A-Za-z0-9_-]+$!', $form_state['values']['name'])) {
    form_set_error('name', t('The name can only consist of lowercase letters, underscores, dashes, and numbers.'));
    return;
  }

  // Check for name collision
  if ($form_state['op'] != 'edit') {
    $plugin = $form_state['plugin'];
    if ($exists = ctools_export_crud_load($plugin['schema'],  $form_state['values']['name'])) {
      form_set_error('name', t('Name @name already exists. Please choose another name or delete the existing item before creating a new one.', array('@name' => $form_state['values']['name'])));
    }
  }

  // Check for uploaded file extension and size  
  if ($form_state['values']['inclusion_type'] == 'file_upload') {
    if (empty($_FILES['files']['name']) && empty($form_state['values']['inclusion_type_inline_js_code'])) {
      form_set_error('inclusion_type_file_upload', t('Please upload javascript file to include'));
    }
    elseif (!empty($_FILES['files']['name'])) {
      $file = new stdClass();
      $file->filesize = $_FILES['files']['size'];
      $file->filename = $_FILES['files']['name'];
      $validate_response = file_validate_extensions($file, JS_INCLUSION_ALLOWED_FILE_EXTENSION);
      if (count($validate_response) > 0) {
        form_set_error('inclusion_type_file_upload', implode('<br/>', $validate_response));
      }
      else {
        $validate_response = file_validate_size($file, JS_INCLUSION_ALLOWED_FILE_SIZE);
        if (count($validate_response) > 0) {
          form_set_error('inclusion_type_file_upload', implode('<br/>', $validate_response));
        }
      }
    }
  }
  elseif ($form_state['values']['inclusion_type'] == 'link_url') {
    $url = $form_state['values']['inclusion_type_link_url'];
    $validate_response = _js_inclusion_validate_extension($url, JS_INCLUSION_ALLOWED_FILE_EXTENSION, 'link');
    if (!empty($validate_response)) {
      form_set_error('inclusion_type_link_url', $validate_response);
    }
    else {
      if (url_is_external($url) && !_js_inclusion_external_url_exist($url)) {
        form_set_error('inclusion_type_link_url', 'Please provide a valid external js link. Not able to find the file.');
      }
      elseif (!url_is_external($url) && !file_exists($url)) {
        form_set_error('inclusion_type_link_url', 'Please provide a valid internal js url. Not able to find the file.');
      }
    }
  }
}

/**
 * Submit handler for the preset edit form.
 */
function js_inclusion_ctools_export_ui_form_submit($form, &$form_state) {

  if ($form_state['values']['inclusion_type'] == 'file_upload') {
    if (isset($_FILES["files"]["tmp_name"]) && !empty($_FILES["files"]["tmp_name"])) {
      $form_state['values']['inclusion_type_inline_js_code'] = file_get_contents($_FILES["files"]["tmp_name"]);
      $form_state['item']->inline_js =  $form_state['values']['inclusion_type_inline_js_code'];
    }
  }
  elseif ($form_state['values']['inclusion_type'] == 'link_url') {
    $form_state['item']->link_url = $form_state['values']['inclusion_type_link_url'];
    $form_state['item']->inline_js = '';
  }
  elseif ($form_state['values']['inclusion_type'] == 'inline_js') {
    $form_state['item']->inline_js = $form_state['values']['inclusion_type_inline_js_code'];
    $form_state['item']->inline_js = strip_tags($form_state['item']->inline_js);
  }

  $form_state['item']->inclusion_type = $form_state['values']['inclusion_type'];
  $form_state['item']->position = $form_state['values']['position'];
  $form_state['item']->weight = $form_state['values']['weight'];
}
