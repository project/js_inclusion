JS Inclusion
===========

Include JS into the particular page output based on the configurable
settings. The JS will be loaded from the below three sources:

  1. Include js file via settings.php
  2. Include js globally via admin UI
  3. Include js in a panel via plugin
  
  
Below are the three ways to include js:
  1. file upload
  2. link
  3. inline js
  
Key Features:
============

1. Multiple js inclusions are allowed
2. We have option to show the inclusion in All pages or selected paths
3. Placement option to include js either in header / footer

JS Loading order:
================

1. JS included from all the sources will be loaded to their appropriate pages.
2. There will be a global configuration to select the default order for inclusion by source (plugin THEN global ui THEN settings.php) which can be turned on or off.
3. The user will be able to supply a weight for each added js (which will be used in drupal_add_js) if the global order configuration is disabled


 Settings.php Configuration:
 ==========================
 
  Example for inline js:
    $conf['settings_php_js_inclusion'][] = array(
      'inline_js' => 'console.log("settings.php");',
      'position' => 'header', 
      'page_visibility_pages' => array('ctools_plugin_example/xxxxx'), 
      'page_visibility' => 1, 
      'weight' => 0
    );
  
  
  Example for Link:  
    $conf['settings_php_js_inclusion'][] = array(
      'link_url' => 'sites/all/modules/js_inclusion/test.js', 
      'position' => 'header', 
      'page_visibility_pages' => array('ctools_plugin_example/xxxxx', 'help/ctools/plugins'), 
      'page_visibility' => 1, 
      'weight' => 0
    );

  
  There is no file upload in settings.php.
